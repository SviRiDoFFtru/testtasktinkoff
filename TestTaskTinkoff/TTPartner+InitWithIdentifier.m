//
//  TTPartner+InitWithIdentifier.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartner+InitWithIdentifier.h"

@implementation TTPartner (InitWithIdentifier)

+(instancetype)initWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [TTPartner fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", identifier];
    [fetchRequest setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    TTPartner *partner = [[context executeFetchRequest:fetchRequest error:nil] firstObject];
    return partner;
}

@end
