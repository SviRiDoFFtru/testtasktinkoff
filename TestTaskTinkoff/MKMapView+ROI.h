//
//  MKMapView+ROI.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 14.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ROI)

- (MKMapRect)visibleROIplusPaddingInPercents:(NSInteger)percents;

@end
