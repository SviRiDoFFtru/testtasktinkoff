//
//  TTPointsRequest.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTPointsRequest : TTBaseRequest

- (instancetype)initWithLatitude:(double)latitude longitude:(double)longitude partners:(NSArray *)partners radius:(NSInteger)radius;

@end
