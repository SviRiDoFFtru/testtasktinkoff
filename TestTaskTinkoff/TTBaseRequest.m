//
//  TTBaseRequest.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTBaseRequest.h"

@implementation TTBaseRequest

-(NSString *)domainName
{
    return @"https://api.tinkoff.ru/v1/";
}

-(NSString *)service
{
    return nil;
}

-(NSDictionary *)parameters
{
    return nil;
}

@end
