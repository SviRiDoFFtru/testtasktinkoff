//
//  IBaseRequest.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IBaseRequest <NSObject>

//URL
- (NSString *)domainName;

//название метода
- (NSString *)service;

//параметры
- (NSDictionary *)parameters;

@end
