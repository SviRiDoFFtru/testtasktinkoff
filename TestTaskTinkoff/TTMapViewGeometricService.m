//
//  TTMapViewGeometricService.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 13.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTMapViewGeometricService.h"

@implementation TTMapViewGeometricService

CLLocationCoordinate2D getCoordinateFromMapRectPoint(double x, double y) {
    MKMapPoint mapPoint = MKMapPointMake(x, y);
    return MKCoordinateForMapPoint(mapPoint);
}

+ (CLLocationCoordinate2D)getTopLeftCoordinate:(MKMapRect)mRect
{
    return getCoordinateFromMapRectPoint(MKMapRectGetMinX(mRect), mRect.origin.y);
}

+ (CLLocationCoordinate2D)getTopRightCoordinate:(MKMapRect)mRect
{
    return getCoordinateFromMapRectPoint(MKMapRectGetMaxX(mRect), mRect.origin.y);
}

+ (CLLocationCoordinate2D)getBottomLeftCoordinate:(MKMapRect)mRect
{
    return getCoordinateFromMapRectPoint(mRect.origin.x, MKMapRectGetMaxY(mRect));
}

+ (CLLocationCoordinate2D)getBottomRightCoordinate:(MKMapRect)mRect
{
    return getCoordinateFromMapRectPoint(MKMapRectGetMaxX(mRect), MKMapRectGetMaxY(mRect));
}

@end
