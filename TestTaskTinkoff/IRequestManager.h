//
//  IRequestManager.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol IBaseRequest;

@protocol IRequestManager <NSObject>

- (NSURLSessionDataTask *)executeRequest:(id<IBaseRequest>)request
                            onCompletion:(void (^)(NSError *error, id object))completion
                               onFailure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)getImageWithImageName:(NSString *)imageName onCompletion:(void (^)(UIImage *))completion onFailure:(void (^)(NSError *))failure;

@end
