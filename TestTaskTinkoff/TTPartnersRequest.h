//
//  TTPartnersRequest.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTPartnersRequest : TTBaseRequest

- (instancetype)initWithAccountType:(NSString *)accountType;

@end
