//
//  ViewController.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "ViewController.h"

//Model
#import "TTPoint+CoreDataClass.h"
#import "TTPartner+CoreDataClass.h"
#import "TTPartner+InitWithDictionary.h"
#import "TTPartner+InitWithIdentifier.h"
#import "TTPoint+InitWithDictionary.h"
#import "TTPoint+InitWithIdentifier.h"

//MapModel
#import "TTPointAnnotation.h"

//UI
#import "TTPinAnnotationView.h"

//Category
#import "MKMapView+GetRadius.h"
#import "MKMapView+ROI.h"
#import "NSSortDescriptor+pointsSortDescriptor.h"

//Dependencies
#import "TTStorage.h"
#import "TTRequestManager.h"
#import "TTMapViewGeometricService.h"

//Requests
#import "TTPartnersRequest.h"
#import "TTPointsRequest.h"

//frameworks
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) TTRequestManager *requestManager;

@property (strong, nonatomic) NSURLSessionDataTask *pointsDataTask;
@property (strong, nonatomic) NSMutableArray <NSString *> *annotationIds;
@property (strong, nonatomic) NSOperationQueue *operationQueue;

@property (assign, nonatomic) BOOL isLoadedPartners;

@end

@implementation ViewController

#pragma mark - ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureLocationManager];
    [self configureRequestManager];
    [self configureMapView];
    [self configureData];
    
    [self configureOperationQueue];
    [self loadPartnersFromServer];
}


#pragma mark - LocationManager

- (void)configureLocationManager
{
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}


#pragma mark - RequestManager

- (void)configureRequestManager
{
    self.requestManager = [[TTRequestManager alloc] initWithSession:[NSURLSession sharedSession]];
}


#pragma mark - Server

- (void)loadPartnersFromServer
{
    __weak typeof(self) weakSelf = self;
    TTPartnersRequest *partnersRequest = [[TTPartnersRequest alloc] initWithAccountType:@"Credit"];
    [self.requestManager executeRequest:partnersRequest onCompletion:^(NSError *error, id object) {
        
        NSManagedObjectContext *backgroundContext = [[TTStorage sharedInstance] getContextForBGTask];
        NSArray <NSDictionary *> *dataArray = object[@"payload"];
        [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            TTPartner *partner = [TTPartner initWithIdentifier:obj[@"id"] inContext:backgroundContext];
            if (!partner) {
                partner = [TTPartner initWithDictionary:obj inContext:backgroundContext];
            }
            
        }];
        
        [[TTStorage sharedInstance] saveContextForBGTask:backgroundContext];
        
        weakSelf.isLoadedPartners = YES;
        [weakSelf loadPointsFromServer];
        
    } onFailure:^(NSError *error) {
        
    }];
}

- (void)loadPointsFromServer
{
    MKCoordinateRegion region = self.mapView.region;
    CLLocationDegrees latitude = region.center.latitude;
    CLLocationDegrees longitude = region.center.longitude;
    CLLocationDistance radius = [self.mapView getRadius];
    
    __weak typeof(self) weakSelf = self;
    TTPointsRequest *pointsRequest = [[TTPointsRequest alloc] initWithLatitude:latitude longitude:longitude partners:nil radius:(NSInteger)radius];
    self.pointsDataTask = [self.requestManager executeRequest:pointsRequest onCompletion:^(NSError *error, id object) {
        
        NSBlockOperation *operation = [[NSBlockOperation alloc] init];
        __weak NSBlockOperation *weakOperation = operation;
        [operation addExecutionBlock:^{
            NSManagedObjectContext *backgroundContext = [[TTStorage sharedInstance] getContextForBGTask];
            NSArray <NSDictionary *> *dataArray = object[@"payload"];
            
            NSMutableArray *markers = [NSMutableArray new];
            [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([weakOperation isCancelled]) {
                    *stop = YES;
                } else {
                    if (obj[@"externalId"]) {
                        TTPoint *point = [TTPoint initWithIdentifier:obj[@"externalId"] inContext:backgroundContext];
                        if (!point) {
                            point = [TTPoint initWithDictionary:obj inContext:backgroundContext];
                            
                            TTPointAnnotation *pointAnnotation = [self createAnnotationWithPoint:point];
                            [markers addObject:pointAnnotation];
                            [weakSelf.annotationIds addObject:point.externalId];
                        }
                    }
                }
            }];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (![weakOperation isCancelled]) {
                    [weakSelf.mapView addAnnotations:markers];
                }
            }];
            
            [[TTStorage sharedInstance] saveContextForBGTask:backgroundContext];
            
        }];
        
        [weakSelf.operationQueue addOperation:operation];
        
    } onFailure:nil];
    
}

- (void)killLoadPointsTask
{
    if (self.pointsDataTask.state == NSURLSessionTaskStateRunning) {
        [self.pointsDataTask cancel];
    }
}


#pragma mark - Data

- (void)configureData
{
    self.annotationIds = [NSMutableArray new];
}


#pragma mark - OperationQueue

- (void)configureOperationQueue
{
    self.operationQueue = [[NSOperationQueue alloc] init];
    self.operationQueue.name = @"AddAnnotationsQueue";
    self.operationQueue.maxConcurrentOperationCount = 1;
    self.operationQueue.suspended = NO;
}

#pragma mark - UI

- (void)configureMapView
{
    self.mapView.delegate = self;

    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = YES;
    
    CLLocationCoordinate2D moscowPosition = CLLocationCoordinate2DMake(55.755773, 37.617761);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(moscowPosition, 10000, 10000);
    [self.mapView setRegion:region animated:NO];
}


#pragma mark - Actions

- (IBAction)zoomIn:(id)sender
{
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta /= 2.0;
    region.span.longitudeDelta /= 2.0;
    [self.mapView setRegion:region animated:NO];
}

- (IBAction)zoomOut:(id)sender {
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta  = MIN(region.span.latitudeDelta  * 2.0, 180.0);
    region.span.longitudeDelta = MIN(region.span.longitudeDelta * 2.0, 180.0);
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)showUserLocation:(id)sender {
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.locationManager.location.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}


#pragma mark - MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    static BOOL isFirstRegionDidChange = YES;
    if (isFirstRegionDidChange) {
        isFirstRegionDidChange = NO;
    } else {
        [self killLoadPointsTask];
        [self.operationQueue cancelAllOperations];
        [self removeInvisibleAnnotations];
        
        [self addVisibleAnnotationsFromCache];
        
        if (_isLoadedPartners) {
            [self loadPointsFromServer];
        }
    }
}

-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    static BOOL isFirstFinishLoadingMap = YES;
    if (isFirstFinishLoadingMap) {
        [self addVisibleAnnotationsFromCache];
        isFirstFinishLoadingMap = NO;
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else {
        static NSString * const kPinAnnotationIdentifier = @"TTPinAnnotationView";
        TTPointAnnotation *pointAnnotation = (TTPointAnnotation *)annotation;
        TTPinAnnotationView *annotationView = (TTPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
        if (!annotationView) {
            annotationView = [[TTPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:kPinAnnotationIdentifier];
        }
        if (!annotationView.imageView.image) {
            [self.requestManager getImageWithImageName:pointAnnotation.imageName onCompletion:^(UIImage *image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    annotationView.imageView.image = image;
                });
                
            } onFailure:^(NSError *error) {
                
            }];
        }
        
        return annotationView;
    }
}




#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    static BOOL isFirstUpdateLocation = YES;
    if (isFirstUpdateLocation) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([locations firstObject].coordinate, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        isFirstUpdateLocation = NO;
    }
}


#pragma mark - Helpers

- (void)addVisibleAnnotationsFromCache
{
    MKMapRect visibleMapRect = [self.mapView visibleROIplusPaddingInPercents:0];
    CLLocationCoordinate2D bottomLeft = [TTMapViewGeometricService getBottomLeftCoordinate:visibleMapRect];
    CLLocationCoordinate2D topRight = [TTMapViewGeometricService getTopRightCoordinate:visibleMapRect];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latitude > %f AND latitude < %f AND longitude > %f AND longitude < %f", bottomLeft.latitude, topRight.latitude, bottomLeft.longitude, topRight.longitude];
    NSManagedObjectContext *defaultManagedObjectContext = [[TTStorage sharedInstance] defaultManagedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(TTPoint.class)];
    fetchRequest.predicate = predicate;
    NSMutableArray <TTPoint *> *fetchedPoints = [NSMutableArray arrayWithArray:[defaultManagedObjectContext executeFetchRequest:fetchRequest error:nil]];
    if (fetchedPoints.count > 400) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor pointsSortDescriptorWithCoordinate:self.mapView.centerCoordinate];
        [fetchedPoints sortUsingDescriptors:@[sortDescriptor]];
        [fetchedPoints removeObjectsInRange:NSMakeRange(400, fetchedPoints.count - 400)];
    }
    NSMutableArray <TTPointAnnotation *> *markers = [NSMutableArray new];
    for (TTPoint *point in fetchedPoints) {
        if (![self.annotationIds containsObject:point.externalId]) {
            TTPointAnnotation *marker = [self createAnnotationWithPoint:point];
            [markers addObject:marker];
            [self.annotationIds addObject:point.externalId];
        }
    }
    [self.mapView addAnnotations:markers];
}

- (TTPointAnnotation *)createAnnotationWithPoint:(TTPoint *)point
{
    TTPointAnnotation *marker = [[TTPointAnnotation alloc] init];
    marker.coordinate = CLLocationCoordinate2DMake(point.latitude, point.longitude);
    marker.pointId = point.externalId;
    marker.imageName = point.partner.picture;
    marker.title = point.partner.name;
    
    return marker;
}

- (void)removeAllAnnotations
{
    id userLocation = [self.mapView userLocation];
    NSMutableArray <id<MKAnnotation>> *pins = [[NSMutableArray alloc] initWithArray:self.mapView.annotations];
    if (userLocation != nil) {
        [pins removeObject:userLocation];
    }
    
    [self.mapView removeAnnotations:pins];
    [self.annotationIds removeAllObjects];
}

- (void)removeInvisibleAnnotations
{
    MKMapRect visibleROI = [self.mapView visibleROIplusPaddingInPercents:0];
    NSSet *inRectAnnotations = [self.mapView annotationsInMapRect:visibleROI];
    
    for (TTPointAnnotation *annotation in self.mapView.annotations) {
        if ([annotation isKindOfClass:[MKUserLocation class]]) {
            continue;
        }
        if (![inRectAnnotations containsObject:annotation]) {
            [self.annotationIds removeObject:annotation.pointId];
            [self.mapView removeAnnotation:annotation];
        }
    }
}

@end
