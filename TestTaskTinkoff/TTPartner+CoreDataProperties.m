//
//  TTPartner+CoreDataProperties.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 12.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartner+CoreDataProperties.h"

@implementation TTPartner (CoreDataProperties)

+ (NSFetchRequest<TTPartner *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TTPartner"];
}

@dynamic depositionDuration;
@dynamic descriptionPartner;
@dynamic hasLocations;
@dynamic id;
@dynamic isMomentary;
@dynamic limitations;
@dynamic name;
@dynamic picture;
@dynamic pointType;
@dynamic url;
@dynamic points;

@end
