//
//  TTPoint+CoreDataProperties.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 12.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPoint+CoreDataProperties.h"

@implementation TTPoint (CoreDataProperties)

+ (NSFetchRequest<TTPoint *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TTPoint"];
}

@dynamic addressInfo;
@dynamic externalId;
@dynamic fullAddress;
@dynamic latitude;
@dynamic longitude;
@dynamic phones;
@dynamic workHours;
@dynamic partner;

@end
