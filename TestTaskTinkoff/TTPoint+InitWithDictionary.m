//
//  TTPoint+InitWithDictionary.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPoint+InitWithDictionary.h"
#import "TTPartner+InitWithIdentifier.h"

@implementation TTPoint (InitWithDictionary)

+(instancetype)initWithDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context
{
    TTPoint *point = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(TTPoint.class) inManagedObjectContext:context];
    
    NSDictionary *location = dictionary[@"location"];
    point.latitude = [location[@"latitude"] doubleValue];
    point.longitude = [location[@"longitude"] doubleValue];
    point.workHours = dictionary[@"workHours"];
    point.phones = dictionary[@"phones"];
    point.addressInfo = dictionary[@"addressInfo"];
    point.fullAddress = dictionary[@"fullAddress"];
    point.externalId = dictionary[@"externalId"];
    
    TTPartner *partner = [TTPartner initWithIdentifier:dictionary[@"partnerName"] inContext:context];
    point.partner = partner;
    
    return point;
}

@end
