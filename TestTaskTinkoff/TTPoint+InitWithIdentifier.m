//
//  TTPoint+InitWithIdentifier.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPoint+InitWithIdentifier.h"

@implementation TTPoint (InitWithIdentifier)

+ (instancetype)initWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [TTPoint fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"externalId = %@", identifier];
    [fetchRequest setPredicate:predicate];
    TTPoint *point = [[context executeFetchRequest:fetchRequest error:nil] firstObject];
    return point;
}

@end
