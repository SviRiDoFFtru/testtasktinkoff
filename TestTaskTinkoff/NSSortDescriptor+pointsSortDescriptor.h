//
//  NSSortDescriptor+pointsSortDescriptor.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 14.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface NSSortDescriptor (pointsSortDescriptor)

+ (NSSortDescriptor *)pointsSortDescriptorWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
