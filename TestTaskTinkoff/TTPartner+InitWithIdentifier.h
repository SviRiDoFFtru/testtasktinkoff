//
//  TTPartner+InitWithIdentifier.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartner+CoreDataClass.h"

@interface TTPartner (InitWithIdentifier)

+ (instancetype)initWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;

@end
