//
//  TTPoint+InitWithIdentifier.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPoint+CoreDataClass.h"

@interface TTPoint (InitWithIdentifier)

+ (instancetype)initWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;

@end
