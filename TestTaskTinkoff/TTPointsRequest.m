//
//  TTPointsRequest.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPointsRequest.h"

@interface TTPointsRequest ()

@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (assign, nonatomic) double radius;
@property (strong, nonatomic) NSArray *partners;

@end

@implementation TTPointsRequest

#pragma mark - Init

-(instancetype)initWithLatitude:(double)latitude longitude:(double)longitude partners:(NSArray <NSString *> *)partners radius:(NSInteger)radius
{
    self = [super init];
    
    if (self) {
        _latitude = latitude;
        _longitude = longitude;
        _radius = radius;
        _partners = partners;
    }
    
    return self;
}


#pragma mark - IBaseRequest

-(NSString *)service
{
    return @"deposition_points";
}

-(NSDictionary *)parameters
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    
    dic[@"latitude"] = @(self.latitude);
    dic[@"longitude"] = @(self.longitude);
    dic[@"radius"] = @(self.radius);
    
    if (self.partners) {
        dic[@"partners"] = self.partners;
    }
    
    return [dic copy];
}

@end
