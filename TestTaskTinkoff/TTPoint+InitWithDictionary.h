//
//  TTPoint+InitWithDictionary.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPoint+CoreDataClass.h"

@interface TTPoint (InitWithDictionary)

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context;

@end
