//
//  TTBaseRequest.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "IBaseRequest.h"

@interface TTBaseRequest : NSObject <IBaseRequest>

@end
