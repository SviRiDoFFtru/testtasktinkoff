//
//  TTPointAnnotation.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 13.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TTPointAnnotation : MKPointAnnotation

@property (strong, nonatomic) NSString *pointId;
@property (strong, nonatomic) NSString *imageName;

@end
