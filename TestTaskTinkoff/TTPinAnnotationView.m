//
//  TTPinAnnotationView.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 14.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPinAnnotationView.h"

@implementation TTPinAnnotationView

-(instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.frame = CGRectMake(0, 0, 20, 20);
        self.backgroundColor = [UIColor clearColor];
        self.canShowCallout = YES;
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _imageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_imageView];
    }
    
    return self;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    
    self.imageView.image = nil;
    self.annotation = nil;
}

@end
