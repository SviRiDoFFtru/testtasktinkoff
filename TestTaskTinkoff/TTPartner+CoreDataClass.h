//
//  TTPartner+CoreDataClass.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 12.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TTPoint;

NS_ASSUME_NONNULL_BEGIN

@interface TTPartner : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TTPartner+CoreDataProperties.h"
