//
//  TTStorage.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface TTStorage : NSObject

//Stack
@property (readonly, strong, nonatomic) NSManagedObjectContext *defaultManagedObjectContext;

//Init
+ (TTStorage *)sharedInstance;

//BackgroundContext
- (NSManagedObjectContext *)getContextForBGTask;

//Save
- (void)saveContextForBGTask:(NSManagedObjectContext *)bgTaskContext;
- (void)saveDefaultContextWithWait:(BOOL)wait;

@end
