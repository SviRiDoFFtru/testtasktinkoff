//
//  TTPartner+InitWithDictionary.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 10.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartner+CoreDataClass.h"

@interface TTPartner (InitWithDictionary)

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context;

@end
