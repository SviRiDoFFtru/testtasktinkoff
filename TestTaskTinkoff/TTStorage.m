//
//  TTStorage.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTStorage.h"

@interface TTStorage ()

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSManagedObjectContext *daddyManagedObjectContext;

@end

@implementation TTStorage

@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize defaultManagedObjectContext = _defaultManagedObjectContext;

#pragma mark - Init

+ (TTStorage *)sharedInstance
{
    static TTStorage *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        manager = [[TTStorage alloc] init];
    });
    
    return manager;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self createCoreDataStack];
    }
    
    return self;
}


#pragma mark - CoreData Stack

- (void)createCoreDataStack
{
    [self daddyManagedObjectContext];
    [self defaultManagedObjectContext];
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator == nil)
    {
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TestTaskTinkoff.sqlite"];
        NSError *error = nil;
        NSString *failureReason = @"There was an error creating or loading the application's saved data.";
        
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
        {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
            abort();
        }
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel == nil)
    {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TestTaskTinkoff" withExtension:@"momd"];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    
    return _managedObjectModel;
}

- (NSManagedObjectContext *)daddyManagedObjectContext
{
    if (_daddyManagedObjectContext == nil) {
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        
        if (coordinator) {
            _daddyManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            _daddyManagedObjectContext.undoManager = nil;
            _daddyManagedObjectContext.persistentStoreCoordinator = coordinator;
        }
    }
    
    return _daddyManagedObjectContext;
}

- (NSManagedObjectContext *)defaultManagedObjectContext
{
    if (_defaultManagedObjectContext == nil) {
        
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        
        if (coordinator) {
            _defaultManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            _defaultManagedObjectContext.undoManager = nil;
            _defaultManagedObjectContext.parentContext = _daddyManagedObjectContext;
        }
    }
    
    return _defaultManagedObjectContext;
}

- (NSManagedObjectContext *)getContextForBGTask
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.undoManager = nil;
    context.parentContext = _defaultManagedObjectContext;
    
    return context;
}


#pragma mark - Save

- (void)saveContextForBGTask:(NSManagedObjectContext *)bgTaskContext {
    if (bgTaskContext.hasChanges) {
        [bgTaskContext performBlockAndWait:^{
            NSError *error = nil;
            [bgTaskContext save:&error];
            if (error) {
                NSLog(@"%@", [error localizedDescription]);
            }
        }];
    }
    [self saveDefaultContextWithWait:YES];
}

- (void)saveDefaultContextWithWait:(BOOL)wait {
    
    if (_defaultManagedObjectContext.hasChanges) {
        [_defaultManagedObjectContext performBlockAndWait:^{
            NSError *error = nil;
            [_defaultManagedObjectContext save:&error];
        }];
    }
    
    void (^saveDaddyContext)(void) = ^{
        NSError *error = nil;
        [_daddyManagedObjectContext save:&error];
    };
    
    if (_daddyManagedObjectContext.hasChanges) {
        if (wait) {
            [_daddyManagedObjectContext performBlockAndWait:saveDaddyContext];
        } else {
            [_daddyManagedObjectContext performBlock:saveDaddyContext];
        }
    }
}


#pragma mark - Helpers

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
