//
//  main.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
