//
//  TTPartnersRequest.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartnersRequest.h"

@interface TTPartnersRequest ()

@property (strong, nonatomic) NSString *accountType;

@end

@implementation TTPartnersRequest

#pragma mark - Init

-(instancetype)initWithAccountType:(NSString *)accountType
{
    self = [super init];
    
    if (self) {
        _accountType = accountType;
    }
    
    return self;
}


#pragma mark - IBaseRequest

-(NSString *)service
{
    return @"deposition_partners";
}

-(NSDictionary *)parameters
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    
    if (self.accountType) {
        dic[@"accountType"] = self.accountType;
    }
    
    return dic;
}

@end
