//
//  MKMapView+GetRadius.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (GetRadius)

- (CLLocationDistance)getRadius;

@end
