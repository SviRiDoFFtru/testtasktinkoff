//
//  TTPartner+InitWithDictionary.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 10.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartner+InitWithDictionary.h"

@implementation TTPartner (InitWithDictionary)

+(instancetype)initWithDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context
{
    TTPartner *partner = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(TTPartner.class) inManagedObjectContext:context];
    
    partner.id = dictionary[@"id"];
    partner.name = dictionary[@"name"];
    partner.picture = dictionary[@"picture"];
    partner.url = dictionary[@"url"];
    partner.hasLocations = [dictionary[@"hasLocations"] boolValue];
    partner.isMomentary = [dictionary[@"isMomentary"] boolValue];
    partner.depositionDuration = dictionary[@"depositionDuration"];
    partner.limitations = dictionary[@"limitations"];
    partner.pointType = dictionary[@"pointType"];
    
    return partner;
}

@end
