//
//  MKMapView+ROI.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 14.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "MKMapView+ROI.h"

@implementation MKMapView (ROI)

-(MKMapRect)visibleROIplusPaddingInPercents:(NSInteger)percents
{
    MKMapRect visibleRect = self.visibleMapRect;
    double widthDelta = visibleRect.size.width * percents / 100.f;
    double heightDelta = visibleRect.size.height * percents / 100.f;
    double originX = visibleRect.origin.x - widthDelta;
    double originY = visibleRect.origin.y - heightDelta;
    double width = visibleRect.size.width + 2 * widthDelta;
    double height = visibleRect.size.height + 2 * heightDelta;
    MKMapRect resultRect = MKMapRectMake(originX, originY, width, height);
    return resultRect;
}

@end
