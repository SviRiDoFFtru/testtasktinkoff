//
//  TTPoint+CoreDataProperties.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 12.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPoint+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TTPoint (CoreDataProperties)

+ (NSFetchRequest<TTPoint *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *addressInfo;
@property (nullable, nonatomic, copy) NSString *externalId;
@property (nullable, nonatomic, copy) NSString *fullAddress;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nullable, nonatomic, copy) NSString *phones;
@property (nullable, nonatomic, copy) NSString *workHours;
@property (nullable, nonatomic, retain) TTPartner *partner;

@end

NS_ASSUME_NONNULL_END
