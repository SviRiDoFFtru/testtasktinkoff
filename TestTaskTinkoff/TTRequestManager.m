//
//  TTRequestManager.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTRequestManager.h"
#import "IBaseRequest.h"

@interface TTRequestManager ()

@property (strong, nonatomic) NSURLSession *session;

@end

@implementation TTRequestManager

#pragma mark - Init

-(instancetype)initWithSession:(NSURLSession *)session
{
    self = [super init];
    
    if (self) {
        _session = session;
    }
    
    return self;
}


#pragma mark - IRequestManager

-(NSURLSessionDataTask *)executeRequest:(id<IBaseRequest>)request onCompletion:(void (^)(NSError *, id))completion onFailure:(void (^)(NSError *))failure
{
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@%@?", [request domainName], [request service]];
    
    NSDictionary *parameters = [request parameters];
    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [urlString appendString:[NSString stringWithFormat:@"%@=%@&", key, obj]];
    }];
    if ([[urlString substringFromIndex:urlString.length - 1] isEqualToString:@"&"]) {
        urlString = [[urlString substringToIndex:urlString.length - 1] mutableCopy];
    }
    
    NSURL *url = [NSURL URLWithString:[urlString copy]];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            if (failure) {
                failure(error);
            }
        } else {
            if (data) {
                NSError *jsonSerializationError = nil;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonSerializationError];
                completion(jsonSerializationError, json);
            }
        }
    }];
    
    [dataTask resume];
    
    return dataTask;
}

-(NSURLSessionDataTask *)getImageWithImageName:(NSString *)imageName onCompletion:(void (^)(UIImage *))completion onFailure:(void (^)(NSError *))failure
{
    NSString *urlString = [NSString stringWithFormat:@"https://static.tinkoff.ru/icons/deposition-partners-v3/mdpi/%@", imageName];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            if (failure) {
                failure(error);
            }
        } else {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                completion(image);
            }
        }
    }];
    
    [dataTask resume];
    
    return dataTask;
}

@end
