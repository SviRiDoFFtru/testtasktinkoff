//
//  TTRequestManager.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 09.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRequestManager.h"

@interface TTRequestManager : NSObject <IRequestManager>

- (instancetype)initWithSession:(NSURLSession *)session;

@end
