//
//  TTMapViewGeometricService.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 13.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface TTMapViewGeometricService : NSObject

+ (CLLocationCoordinate2D)getTopLeftCoordinate:(MKMapRect)mRect;
+ (CLLocationCoordinate2D)getTopRightCoordinate:(MKMapRect)mRect;
+ (CLLocationCoordinate2D)getBottomLeftCoordinate:(MKMapRect)mRect;
+ (CLLocationCoordinate2D)getBottomRightCoordinate:(MKMapRect)mRect;

@end
