//
//  NSSortDescriptor+pointsSortDescriptor.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 14.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "NSSortDescriptor+pointsSortDescriptor.h"
#import "TTPoint+CoreDataClass.h"

@implementation NSSortDescriptor (pointsSortDescriptor)

+(NSSortDescriptor *)pointsSortDescriptorWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        TTPoint *point1 = (TTPoint *)obj1;
        TTPoint *point2 = (TTPoint *)obj2;
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:point1.latitude longitude:point1.longitude];
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:point2.latitude longitude:point2.longitude];
        CLLocationDistance distance1 = [location distanceFromLocation:location1];
        CLLocationDistance distance2 = [location distanceFromLocation:location2];
        
        if (distance1 < distance2) {
            return NSOrderedAscending;
        } else if (distance1 > distance2) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    return sortDescriptor;
}

@end
