//
//  MKMapView+GetRadius.m
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 11.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "MKMapView+GetRadius.h"

@implementation MKMapView (GetRadius)

-(CLLocationDistance)getRadius
{
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:self.centerCoordinate.latitude longitude:self.centerCoordinate.longitude];
    double topRightLat = self.centerCoordinate.latitude + self.region.span.latitudeDelta/2.f;
    double topRightLon = self.centerCoordinate.longitude + self.region.span.longitudeDelta/2.f;
    CLLocation *topRightLocation = [[CLLocation alloc] initWithLatitude:topRightLat longitude:topRightLon];
    CLLocationDistance distance = [centerLocation distanceFromLocation:topRightLocation];
    
    return distance;
}

@end
