//
//  TTPinAnnotationView.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 14.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TTPinAnnotationView : MKAnnotationView

@property (strong, nonatomic) UIImageView *imageView;

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;

@end
