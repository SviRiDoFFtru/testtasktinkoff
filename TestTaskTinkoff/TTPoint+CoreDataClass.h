//
//  TTPoint+CoreDataClass.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 12.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TTPartner;

NS_ASSUME_NONNULL_BEGIN

@interface TTPoint : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TTPoint+CoreDataProperties.h"
