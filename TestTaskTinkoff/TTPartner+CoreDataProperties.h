//
//  TTPartner+CoreDataProperties.h
//  TestTaskTinkoff
//
//  Created by Alexander Trushin on 12.10.16.
//  Copyright © 2016 Alexander Trushin. All rights reserved.
//

#import "TTPartner+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TTPartner (CoreDataProperties)

+ (NSFetchRequest<TTPartner *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *depositionDuration;
@property (nullable, nonatomic, copy) NSString *descriptionPartner;
@property (nonatomic) BOOL hasLocations;
@property (nullable, nonatomic, copy) NSString *id;
@property (nonatomic) BOOL isMomentary;
@property (nullable, nonatomic, copy) NSString *limitations;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *picture;
@property (nullable, nonatomic, copy) NSString *pointType;
@property (nullable, nonatomic, copy) NSString *url;
@property (nullable, nonatomic, retain) NSSet<TTPoint *> *points;

@end

@interface TTPartner (CoreDataGeneratedAccessors)

- (void)addPointsObject:(TTPoint *)value;
- (void)removePointsObject:(TTPoint *)value;
- (void)addPoints:(NSSet<TTPoint *> *)values;
- (void)removePoints:(NSSet<TTPoint *> *)values;

@end

NS_ASSUME_NONNULL_END
